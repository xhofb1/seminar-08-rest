package cz.fi.muni.pa165.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;

import cz.fi.muni.pa165.entity.Category;
import cz.fi.muni.pa165.entity.Price;
import cz.fi.muni.pa165.entity.Product;
import cz.fi.muni.pa165.enums.Currency;


@Service
public interface ProductService {
	Product findById(Long id);
	List<Product> findAll();
	Product createProduct(Product p);
	void addCategory(Product product, Category category);
	void removeCategory(Product product, Category category);
	void changePrice(Product product, Price newPrice);
	void deleteProduct(Product p);
	BigDecimal getPriceValueInCurrency(Product p, Currency currency);
}
