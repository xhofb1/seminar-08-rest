package cz.fi.muni.pa165.dao;

import java.util.List;

import cz.fi.muni.pa165.entity.Category;

public interface CategoryDao {
	Category findById(Long id);
	void create(Category c);
	void delete(Category c);
	List<Category> findAll();
	Category findByName(String name);
}
