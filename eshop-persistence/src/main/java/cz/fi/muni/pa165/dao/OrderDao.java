package cz.fi.muni.pa165.dao;

import java.util.Date;
import java.util.List;

import cz.fi.muni.pa165.entity.Order;
import cz.fi.muni.pa165.entity.User;
import cz.fi.muni.pa165.enums.OrderState;

public interface OrderDao  {
	void create(Order order);
	List<Order> findAll();
	List<Order> findByUser(User u);
	Order findById(Long id);
	void remove(Order o)  throws IllegalArgumentException;
	List<Order> getOrdersWithState(OrderState state);
	List<Order> getOrdersCreatedBetween(Date start, Date end, OrderState state);
}
