package cz.fi.muni.pa165.dao;

import cz.fi.muni.pa165.entity.OrderItem;

public interface OrderItemDao {
	void create(OrderItem orderItem);
	OrderItem findById(Long id);
	void removeById(Long id);
	void delete(OrderItem orderItem);
}
