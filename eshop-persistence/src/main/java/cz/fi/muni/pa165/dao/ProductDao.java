package cz.fi.muni.pa165.dao;

import java.util.List;

import cz.fi.muni.pa165.entity.Product;

public interface ProductDao {
	void create(Product p);
	Product findById(Long id);
	List<Product> findAll();
	void remove(Product p) throws IllegalArgumentException;
	List<Product> findByName(String namePattern);
}
